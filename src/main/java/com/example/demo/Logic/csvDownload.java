package com.example.demo.Logic;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.entity.CSV;
import com.example.demo.entity.CsvColumn;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

@Controller
public class csvDownload {

	@PostMapping(value = "/details.csv", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE
		      + "; charset=UTF-8; Content-Disposition: attachment")
		  @ResponseBody
		  public Object csvDownloader(@ModelAttribute("csvForm") CSV records) throws JsonProcessingException {
		    List<CsvColumn> csvList = new ArrayList<>();
		    for (int i = 0; i < records.getCategory().size(); i++) { // レコードの数ぶんだけループ回して
		    	if(records.getCategory().size() == 1) {
		    		if(records.getText().size() == 0) {
		    			csvList.add(new CsvColumn(records.getDate().get(i), records.getCalculateName().get(i), records.getCategory().get(i), records.getAmount().get(i), ""));
		    		} else {
		    			csvList.add(new CsvColumn(records.getDate().get(i), records.getCalculateName().get(i), records.getCategory().get(i), records.getAmount().get(i), records.getText().get(i).replaceAll("\\n", " ").replaceAll("\\r", " ")));
		    		}
		    	} else {
		    		csvList.add(new CsvColumn(records.getDate().get(i), records.getCalculateName().get(i), records.getCategory().get(i), records.getAmount().get(i), records.getText().get(i).replaceAll("\\n", " ").replaceAll("\\r", " ")));
		    	}
		    }
		    CsvMapper mapper = new CsvMapper();
		    CsvSchema schema = mapper.schemaFor(CsvColumn.class).withHeader();
		    return mapper.writer(schema).writeValueAsString(csvList);
		  }
}
