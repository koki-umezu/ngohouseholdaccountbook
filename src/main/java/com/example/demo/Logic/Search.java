package com.example.demo.Logic;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.example.demo.entity.CompareExpense;
import com.example.demo.entity.CompareIncome;
import com.example.demo.entity.Expense;
import com.example.demo.entity.Income;
import com.example.demo.entity.User;

public class Search {

	@Autowired
	HttpSession session;

	public static Map<String, Integer> exSearch(List<CompareExpense> compareExpenses, Integer age, String gender) {
		Map<String, Integer> expenseListByAge = new LinkedHashMap<String, Integer>();
		List<Integer> users = new ArrayList<Integer>();
		for (CompareExpense compareExpense : compareExpenses) {

			User userData = new User();
			userData = compareExpense.getExUser();

			if (age == null && !StringUtils.hasText(gender)) {
				collectExData(compareExpense, users, expenseListByAge);
			} else if (!StringUtils.hasText(gender)) {
				if (userData.getAge() == age) {
					collectExData(compareExpense, users, expenseListByAge);
				}
			} else if (age == null) {
				if (userData.getGender().equals(gender)) {
					collectExData(compareExpense, users, expenseListByAge);
				}
			} else {
				if (userData.getGender().equals(gender) && userData.getAge() == age) {
					collectExData(compareExpense, users, expenseListByAge);
				}
			}

		}

		System.out.println(users.size());
		for (String key : expenseListByAge.keySet()) {
			expenseListByAge.put(key, expenseListByAge.get(key) / users.size());
		}
		return expenseListByAge;
	}

	public static void collectExData(CompareExpense compareExpense, List<Integer> users,
			Map<String, Integer> expenseListByAge) {

		if (compareExpense.getExCategory().getId() != 99) {

			if (expenseListByAge.containsKey(compareExpense.getExCategory().getName())) {
				expenseListByAge.put(compareExpense.getExCategory().getName(),
						expenseListByAge.get(compareExpense.getExCategory().getName())
								+ compareExpense.getAmount());
			} else {
				expenseListByAge.put(compareExpense.getExCategory().getName(), compareExpense.getAmount());
			}

			if (!users.contains(compareExpense.getExUser().getId())) {
				users.add(compareExpense.getExUser().getId());
			}

		}

	}

	public static Map<String, Integer> inSearch(List<CompareIncome> compareIncomes, Integer age, String gender) {
		Map<String, Integer> IncomeListByAge = new LinkedHashMap<String, Integer>();
		List<Integer> users = new ArrayList<Integer>();
		for (CompareIncome compareIncome : compareIncomes) {
			User userData = new User();
			userData = compareIncome.getInUser();

			if (age == null && !StringUtils.hasText(gender)) {
				collectInData(compareIncome, users, IncomeListByAge);
			} else if (!StringUtils.hasText(gender)) {
				if (userData.getAge() == age) {
					collectInData(compareIncome, users, IncomeListByAge);
				}
			} else if (age == null) {
				if (userData.getGender().equals(gender)) {
					collectInData(compareIncome, users, IncomeListByAge);
				}
			} else {
				if (userData.getGender().equals(gender) && userData.getAge() == age) {
					collectInData(compareIncome, users, IncomeListByAge);
				}
			}

		}

		for (String key : IncomeListByAge.keySet()) {
			IncomeListByAge.put(key, IncomeListByAge.get(key) / users.size());
		}
		return IncomeListByAge;
	}

	public static void collectInData(CompareIncome compareIncome, List<Integer> users,
			Map<String, Integer> incomeListByAge) {

		if (compareIncome.getInCategory().getId() != 99) {

			if (incomeListByAge.containsKey(compareIncome.getInCategory().getName())) {
				incomeListByAge.put(compareIncome.getInCategory().getName(),
						incomeListByAge.get(compareIncome.getInCategory().getName())
								+ compareIncome.getAmount());
			} else {
				incomeListByAge.put(compareIncome.getInCategory().getName(), compareIncome.getAmount());
			}

			if (!users.contains(compareIncome.getInUser().getId())) {
				users.add(compareIncome.getInUser().getId());
			}

		}

	}

	public static Map<String, Integer> exUserSearch(List<CompareExpense> compareExpenses,Integer userId){
		Map<String, Integer> userExpenseList = new LinkedHashMap<String, Integer>();
		List<Integer> users = new ArrayList<Integer>();
		for (CompareExpense compareExpense : compareExpenses) {
			User userData = new User();
			userData = compareExpense.getExUser();

			if(userData.getId()==userId) {
				collectExData(compareExpense, users, userExpenseList);
			}

		}
		return userExpenseList;
	}

	public static Map<String, Integer> inUserSearch(List<CompareIncome> compareIncomes,Integer userId){
		Map<String, Integer> userIncomeList = new LinkedHashMap<String, Integer>();
		List<Integer> users = new ArrayList<Integer>();
		for (CompareIncome compareIncome : compareIncomes) {
			User userData = new User();
			userData = compareIncome.getInUser();

			if(userData.getId()==userId) {
				collectInData(compareIncome, users, userIncomeList);
			}

		}
		return userIncomeList;
	}

	public static List<Integer> setSearchYear(Expense ex, Income in) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");

		//ユーザー毎の収支データが存在するかチェック
		//存在する場合は日付を収支データの日付を設定
		//存在しない場合は現在の日付を設定
		Date exDate = new Date();
		Date inDate = new Date();
		if (ex != null && in != null) {
			exDate = ex.getDate();
			inDate = in.getDate();
		} else if (ex == null) {
			inDate = in.getDate();
		} else if (in == null) {
			exDate = ex.getDate();
		}

		//支出と収入の日付を比較し古いものを絞込出来る年の下限に設定
		Date serachOldestDate = new Date();
		if (exDate.after(inDate)) {
			serachOldestDate = inDate;
		} else {
			serachOldestDate = exDate;
		}
		String strOldestSearch = sdf.format(serachOldestDate);
		int searchOldestYear = Integer.parseInt(strOldestSearch.substring(0, 4));

		//現在の日付を取得し絞込出来る年の上限に設定
		Date now = new Date();
		String strNewestSearch = sdf.format(now);
		int searchNewestYear = Integer.parseInt(strNewestSearch.substring(0, 4));

		//取得した下限から上限までの範囲をリストに追加する
		List<Integer> searchYears = new ArrayList<Integer>();

		while (searchOldestYear <= searchNewestYear) {
			searchYears.add(searchNewestYear);
			searchNewestYear--;
		}

		return searchYears;
	}
}
