package com.example.demo.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.example.demo.entity.Expense;

@Component
public class BalanceSpecification {

	public Specification<Expense> joinCategory(Integer id){

		return  new Specification<Expense>() {
			@Override
			public javax.persistence.criteria.Predicate toPredicate(Root<Expense> root, CriteriaQuery<?> query,
					CriteriaBuilder cb) {
//				CriteriaQuery<Tuple> query2 = cb.createQuery(Tuple.class);
//				Path<String> string = root.get("exCategory");
//			    javax.persistence.criteria.Expression<Number> sum = cb.sum(root.get("amount"));
//
//			    query2.select(cb.tuple(string, sum))
//		         .groupBy(string);
//
//			    System.out.println(query2.getResultType());


//				query.orderBy(cb.desc(root.get("date")));
//				query.multiselect(cb.tuple(root.get("exCategory"),cb.sum(root.get("amount")))).groupBy(root.get("exCategory"));
				cb.sum(root.get("amount"));
				query.groupBy(root.get("exCategory"));
//				cq.multiselect(stud.get("s_age"),cb.count(stud)).groupBy(stud.get("s_age"));
				return cb.equal(root.get("userId"),1);
//				return cb.notEqual(root.join("exCategory", JoinType.INNER).get("id"), id);
			}
		};

//	public Specification<ExpenseCategory> joinCategory(Integer id){
//
//		return  new Specification<ExpenseCategory>() {
//			@Override
//			public javax.persistence.criteria.Predicate toPredicate(Root<ExpenseCategory> root, CriteriaQuery<?> query,
//					CriteriaBuilder cb) {
//			return cb.equal(root.join("expense", JoinType.LEFT).get("id"), 1);
//			}
//		};
//	}
	}
}
