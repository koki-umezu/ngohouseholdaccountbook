package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="users")
@Getter
@Setter
public class User {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String name;
	@Column
	private String password;
	@Column
	private int age;
	@Column
	private String gender;
	@Column
	private Integer money;
	@Column(name = "created_date", nullable = false, updatable = false, insertable = false)
	private Date createdDate;
	@Column(name = "updated_date", nullable = false, updatable = true, insertable = false)
	private Date updatedDate;

	@PreUpdate
	public void onPrePersist() {
		Date date = new Date();
		this.updatedDate = date;
	}


}
