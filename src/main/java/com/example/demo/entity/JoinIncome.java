package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="register_incomes")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class JoinIncome {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name="userId")
	private int userId;
	@Column(name="amount")
	private int amount;
	@Column(name="date")
	private Date date;
	@Column(name="text")
	private String text;
	@Column(name = "created_date", nullable = false, updatable = false, insertable = false)
	private Date createdDate;
	@Column(name = "updated_date", nullable = false, updatable = true, insertable = false)
	private Date updatedDate;

	@OneToOne
	@JoinColumn(name = "inCategoryId")
	private IncomeCategory inCategory;


	@PreUpdate
	public void onPrePersist() {
		Date date = new Date();
		this.updatedDate = date;
	}

}
