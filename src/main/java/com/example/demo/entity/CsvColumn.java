package com.example.demo.entity;



import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonPropertyOrder({"日付", "収支", "収支カテゴリー","金額","備考"})
@Data
public class CsvColumn {

  @JsonProperty("日付")
  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date date;

  @JsonProperty("収支")
  private String calculateName;

  @JsonProperty("収支カテゴリー")
  private String category;

  @JsonProperty("金額")
  private Integer amount;

  @JsonProperty("備考")
  private String text;

  public CsvColumn () {}

  public CsvColumn (Date date, String calculateName, String category, Integer amount, String text) {
    this.date = date;
    this.calculateName = calculateName;
    this.category = category;
    this.amount = amount;
    this.text = text;
  }
}
