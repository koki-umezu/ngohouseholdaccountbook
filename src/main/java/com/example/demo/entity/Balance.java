package com.example.demo.entity;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Balance {

	private int amount;

	private Date date;

	private String text;

	private String category;

	private int balanceId;

	private int categoryId;


}
