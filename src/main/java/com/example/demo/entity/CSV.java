package com.example.demo.entity;

import java.util.Date;
import java.util.List;

import lombok.Data;
@Data
public class CSV {

	List<Date> date;

	List<String> calculateName;

	List<String> category;

	List<Integer> amount;

	List<String> text;
}
