package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="register_expenses")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Expense {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private int exCategoryId;
	@Column(name="user_id")
	private int userId;
	@Column(name="amount")
	private int amount;
	@Column(name="date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;
	@Column(name="text")
	private String text;
	@Column(name = "created_date", nullable = false, updatable = false, insertable = false)
	private Date createdDate;
	@Column(name = "updated_date", nullable = false, updatable = true, insertable = false)
	private Date updatedDate;

	@PreUpdate
	public void onPrePersist() {
		Date date = new Date();
		this.updatedDate = date;
	}




}
