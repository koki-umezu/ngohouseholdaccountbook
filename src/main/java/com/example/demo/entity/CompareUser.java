package com.example.demo.entity;

import java.util.Map;

import lombok.Data;

@Data
public class CompareUser {

	private int userId;

	private Map<String, Integer> categoryAmount;

	private int age;

}
