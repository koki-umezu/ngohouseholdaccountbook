package com.example.demo.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.entity.Expense;
import com.example.demo.entity.ExpenseCategory;
import com.example.demo.entity.Income;
import com.example.demo.entity.IncomeCategory;
import com.example.demo.entity.User;
import com.example.demo.service.ExpenseCategoryService;
import com.example.demo.service.ExpenseService;
import com.example.demo.service.IncomeCategoryService;
import com.example.demo.service.IncomeService;
import com.example.demo.service.UserService;

@Controller
//@RequestMapping("file/upload")
public class FileUploadRestController {

	@Autowired
	UserService userService;
	@Autowired
	ExpenseService expenseService;
	@Autowired
	IncomeService incomeService;
	@Autowired
	ExpenseCategoryService expenseCategoryService;
	@Autowired
	IncomeCategoryService incomeCategoryService;

	@Autowired
	HttpSession session;

	@PostMapping(value = "/expensefile", params = "upload_file")
	public String uploadExpenseFile(@RequestParam("file") MultipartFile uploadFile) {
		List<User> user = (List<User>) session.getAttribute("loginUser");
		if(uploadFile.isEmpty()){
			int id = user.get(0).getId();
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("ファイルが選択されておりません");
			session.setAttribute("errorMessages", errorMessages);
			return "redirect:/NewCalculate/" + id;
		}
		List<Expense> expenseList = new ArrayList<Expense>();
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(uploadFile.getInputStream(), StandardCharsets.UTF_8))) {
			String line;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			int lineNumber = 0;
			while ((line = br.readLine()) != null) {
				final String[] split = line.split(",");
				int count = 0;
				lineNumber = lineNumber + 1;
				Expense expense;
				List<String> errorMessages = new ArrayList<String>();

				if (split.length == 3) {
					// エラー処理
					// カテゴリIDが数字でない場合
					if (!split[0].matches("^[0-9]+$")) {
						errorMessages.add("カテゴリIDが不正です");
					}
					// 金額が空白の場合または半角数字でない場合
					if (!(split[1].matches("^[0-9]+$"))) {
						errorMessages.add("金額には半角数字を入力してください");
					}
				} else if (split.length == 4) {
					// エラー処理
					// カテゴリIDが数字でない場合
					if (!split[0].matches("^[0-9]+$")) {
						errorMessages.add("カテゴリIDが不正です");
					}
					// 金額が空白の場合または半角数字でない場合
					if (!(split[1].matches("^[0-9]+$"))) {
						errorMessages.add("金額には半角数字を入力してください");
					}
					// 備考に51文字以上入力された場合
					if (split[3].length() > 50) {
						errorMessages.add("備考は50文字以内で入力してください");
					}
				} else {
					errorMessages.add("必要な値が入力されていません");
				}

				if (errorMessages.size() != 0) {
					errorMessages.add(lineNumber + "行目にエラーがあります");
					errorMessages.add("ファイルの読み込みに失敗しました");
					session.setAttribute("calculateId", 0);
					session.setAttribute("errorMessages", errorMessages);
					int id = user.get(0).getId();
					return "redirect:/NewCalculate/" + id;
				}

				// カテゴリーを全件取得
				List<ExpenseCategory> category = expenseCategoryService.findAllExpenseCategory();
				for (int i = 0; i < category.size(); i++) {
					if ((split[0]).equals(String.valueOf(category.get(i).getId()))) {
						try {
							if (split.length == 4) {
								expense = Expense.builder().id(0).exCategoryId(Integer.parseInt(split[0]))
										.userId(user.get(0).getId()).amount(Integer.parseInt(split[1]))
										.date(sdf.parse(split[2])).text(split[3]).build();
								expenseList.add(expense);
								count = count + 1;
							} else {
								expense = Expense.builder().id(0).exCategoryId(Integer.parseInt(split[0]))
										.userId(user.get(0).getId()).amount(Integer.parseInt(split[1]))
										.date(sdf.parse(split[2])).build();
								expenseList.add(expense);
								count = count + 1;
							}
						} catch (NumberFormatException | ParseException e) {
							// TODO 自動生成された catch ブロック
							e.printStackTrace();
							errorMessages.add(lineNumber + "行目にエラーがあります");
							errorMessages.add("ファイルの読み込みに失敗しました");
							session.setAttribute("calculateId", 0);
							session.setAttribute("errorMessages", errorMessages);
							int id = user.get(0).getId();
							return "redirect:/NewCalculate/" + id;
						}
					}
				}
				if (count == 0) {
					errorMessages.add("存在しないカテゴリーIDです");
					errorMessages.add(lineNumber + "行目にエラーがあります");
					errorMessages.add("ファイルの読み込みに失敗しました");
					session.setAttribute("calculateId", 0);
					session.setAttribute("errorMessages", errorMessages);
					int id = user.get(0).getId();
					return "redirect:/NewCalculate/" + id;
				}
			}
		} catch (IOException e) {
			throw new RuntimeException("ファイルが読み込めません", e);
		}

		expenseService.saveExpenseList(expenseList);

		// ユーザーデータの取得
		User calculateUser = userService.editUser(user.get(0).getId());
		// 貯蓄額の更新
		int userMoney = calculateUser.getMoney();
		int amount = 0;
		for (int i = 0; i < expenseList.size(); i++) {
			if(expenseList.get(i).getExCategoryId()!=99) {
				amount = amount + expenseList.get(i).getAmount();
			}
		}
		userMoney = userMoney - amount;
		calculateUser.setMoney(userMoney);
		userService.saveUser(calculateUser);

		return "redirect:/";
	}

	@PostMapping(value = "/incomefile", params = "upload_file")
	public String uploadIncomeFile(@RequestParam("file") MultipartFile uploadFile) {
		List<User> user = (List<User>) session.getAttribute("loginUser");
		if(uploadFile.isEmpty()){
			int id = user.get(0).getId();
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("ファイルが選択されておりません");
			session.setAttribute("errorMessages", errorMessages);
			return "redirect:/NewCalculate/" + id;
		}

		List<Income> incomeList = new ArrayList<Income>();
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(uploadFile.getInputStream(), StandardCharsets.UTF_8))) {
			String line;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			int lineNumber = 0;
			while ((line = br.readLine()) != null) {
				final String[] split = line.split(",");
				int count = 0;
				lineNumber = lineNumber + 1;
				Income income;

				List<String> errorMessages = new ArrayList<String>();

				if (split.length == 3) {
					// エラー処理
					// カテゴリIDが数字でない場合
					if (!split[0].matches("^[0-9]+$")) {
						errorMessages.add("カテゴリIDが不正です");
					}
					// 金額が空白の場合または半角数字でない場合
					if (!(split[1].matches("^[0-9]+$"))) {
						errorMessages.add("金額には半角数字を入力してください");
					}
				} else if (split.length == 4) {
					// エラー処理
					// カテゴリIDが数字でない場合
					if (!split[0].matches("^[0-9]+$")) {
						errorMessages.add("カテゴリIDが不正です");
					}
					// 金額が空白の場合または半角数字でない場合
					if (!(split[1].matches("^[0-9]+$"))) {
						errorMessages.add("金額には半角数字を入力してください");
					}
					// 備考に51文字以上入力された場合
					if (split[3].length() > 50) {
						errorMessages.add("備考は50文字以内で入力してください");
					}
				} else {
					errorMessages.add("必要な値が入力されていません");
				}

				if (errorMessages.size() != 0) {
					errorMessages.add(lineNumber + "行目にエラーがあります");
					errorMessages.add("ファイルの読み込みに失敗しました");
					session.setAttribute("calculateId", 1);
					session.setAttribute("errorMessages", errorMessages);
					int id = user.get(0).getId();
					return "redirect:/NewCalculate/" + id;
				}

				// カテゴリーを全件取得
				List<IncomeCategory> category = incomeCategoryService.findAllIncomeCategory();
				for (int i = 0; i < category.size(); i++) {
					if ((split[0]).equals(String.valueOf(category.get(i).getId()))) {
						try {
							if (split.length == 4) {
								income = Income.builder().id(0).inCategoryId(Integer.parseInt(split[0]))
										.userId(user.get(0).getId()).amount(Integer.parseInt(split[1]))
										.date(sdf.parse(split[2])).text(split[3]).build();
								incomeList.add(income);
								count = count + 1;
							} else {
								income = Income.builder().id(0).inCategoryId(Integer.parseInt(split[0]))
										.userId(user.get(0).getId()).amount(Integer.parseInt(split[1]))
										.date(sdf.parse(split[2])).build();
								incomeList.add(income);
								count = count + 1;
							}
						} catch (NumberFormatException | ParseException e) {
							// TODO 自動生成された catch ブロック
							e.printStackTrace();
							errorMessages.add(lineNumber + "行目にエラーがあります");
							errorMessages.add("ファイルの読み込みに失敗しました");
							session.setAttribute("calculateId", 1);
							session.setAttribute("errorMessages", errorMessages);
							int id = user.get(0).getId();
							return "redirect:/NewCalculate/" + id;
						}
					}
				}
				if (count == 0) {
					errorMessages.add("存在しないカテゴリーIDです");
					errorMessages.add(lineNumber + "行目にエラーがあります");
					errorMessages.add("ファイルの読み込みに失敗しました");
					session.setAttribute("calculateId", 1);
					session.setAttribute("errorMessages", errorMessages);
					int id = user.get(0).getId();
					return "redirect:/NewCalculate/" + id;
				}
			}
		} catch (IOException e) {
			throw new RuntimeException("ファイルが読み込めません", e);
		}
		incomeService.saveIncomeList(incomeList);

		// ユーザーデータの取得
		User calculateUser = userService.editUser(user.get(0).getId());
		// 貯蓄額の更新
		int userMoney = calculateUser.getMoney();
		int amount = 0;
		for (int i = 0; i < incomeList.size(); i++) {
			if (incomeList.get(i).getInCategoryId() != 99) {
				amount = amount + incomeList.get(i).getAmount();
			}
		}
		userMoney = userMoney + amount;
		calculateUser.setMoney(userMoney);
		userService.saveUser(calculateUser);

		return "redirect:/";
	}
}
