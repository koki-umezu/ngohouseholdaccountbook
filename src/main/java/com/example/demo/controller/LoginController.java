package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.service.LoginService;

@Controller
public class LoginController {
	@Autowired
	LoginService loginService;

	@Autowired
	HttpSession session;

	//ログイン画面表示
	@GetMapping("/Login")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView();
		//画面推移先指定
		mav.setViewName("/Login");
		mav.addObject("errorMessagesLogin", session.getAttribute("errorMessagesLogin"));
		return mav;
	}

	//ログイン機能
	@PostMapping("/login")
	public ModelAndView loginContent(HttpServletRequest request, HttpServletResponse response, Model model,
			Pageable pageable) {

		String name = request.getParameter("name");
		String password = request.getParameter("password");

		if ((name == "") && (password == "")) {
			ModelAndView mav = new ModelAndView();
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("ユーザー名が入力されていません");
			errorMessages.add("パスワードが入力されていません");
			mav.addObject("errorMessagesLogin", errorMessages);
			mav.setViewName("/Login");
			return mav;
		} else if (name == "") {
			ModelAndView mav = new ModelAndView();
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("ユーザー名が入力されていません");
			mav.addObject("errorMessagesLogin", errorMessages);
			mav.setViewName("/Login");
			return mav;
		} else if (password == "") {
			ModelAndView mav = new ModelAndView();
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("パスワードが入力されていません");
			mav.addObject("errorMessagesLogin", errorMessages);
			mav.addObject("formModel", name);
			mav.setViewName("/Login");
			return mav;
		}

		List<User> accountData = loginService.selectAccount(name, password);

		if (accountData.size() == 0) {
			ModelAndView mav = new ModelAndView();
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("ユーザー名またはパスワードが誤っています");
			mav.addObject("errorMessagesLogin", errorMessages);
			mav.addObject("formModel", name);
			mav.setViewName("/Login");
			return mav;
		}

		ModelAndView mav = new ModelAndView();
		mav.addObject("loginUser", accountData);
		// 画面遷移先を指定
		mav.setViewName("redirect:/");
		session.setAttribute("loginUser", accountData);
		session.removeAttribute("errorMessages");
		return mav;

	}
}
