package com.example.demo.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Expense;
import com.example.demo.entity.ExpenseCategory;
import com.example.demo.entity.Income;
import com.example.demo.entity.IncomeCategory;
import com.example.demo.entity.User;
import com.example.demo.service.ExpenseCategoryService;
import com.example.demo.service.ExpenseService;
import com.example.demo.service.IncomeCategoryService;
import com.example.demo.service.IncomeService;
import com.example.demo.service.UserService;

@Controller
public class CalculateController {
	@Autowired
	ExpenseService expenseService;
	@Autowired
	IncomeService incomeService;
	@Autowired
	ExpenseCategoryService expenseCategoryService;
	@Autowired
	IncomeCategoryService incomeCategoryService;
	@Autowired
	UserService userService;
	@Autowired
	HttpSession session;

	// 収支登録画面
	@GetMapping("/NewCalculate/{id}")
	public ModelAndView newContent(@PathVariable String id,
			@RequestParam(defaultValue = "0", name = "calculateId") int calculateId) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		if(!id.matches("^[0-9]+$")) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			mav.setViewName("redirect:/");
			return mav;
		}

		List<User> user = (List<User>) session.getAttribute("loginUser");
		int intId = Integer.parseInt(id);

		if(intId != user.get(0).getId()) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			mav.setViewName("redirect:/");
			return mav;
		}

		if(session.getAttribute("calculateId") != null) {
			if((int) session.getAttribute("calculateId") == 0) {
				Expense expense = new Expense(); // form用の空のentityを準備
				mav.addObject("expense", expense); // 準備した空のentityを保管
				List<ExpenseCategory> category = expenseCategoryService.findAllExpenseCategory(); // カテゴリーを全件取得
				mav.addObject("category", category);
				mav.addObject("calculateId", session.getAttribute("calculateId"));
				session.removeAttribute("calculateId");
			} else if((int) session.getAttribute("calculateId") == 1) {
				Income income = new Income(); // form用の空のentityを準備
				mav.addObject("income", income); // 準備した空のentityを保管
				List<IncomeCategory> category = incomeCategoryService.findAllIncomeCategory(); // カテゴリーを全件取得
				mav.addObject("category", category);
				mav.addObject("calculateId", session.getAttribute("calculateId"));
				session.removeAttribute("calculateId");
			}
		} else {
			if(calculateId == 0) {
				Expense expense = new Expense(); // form用の空のentityを準備
				mav.addObject("expense", expense); // 準備した空のentityを保管
				List<ExpenseCategory> category = expenseCategoryService.findAllExpenseCategory(); // カテゴリーを全件取得
				mav.addObject("category", category);
				mav.addObject("calculateId", calculateId);
			} else {
				Income income = new Income(); // form用の空のentityを準備
				mav.addObject("income", income); // 準備した空のentityを保管
				List<IncomeCategory> category = incomeCategoryService.findAllIncomeCategory(); // カテゴリーを全件取得
				mav.addObject("category", category);
				mav.addObject("calculateId", calculateId);
			}
		}

		// セッションからエラーメッセージを取得してセッションから取り除く
		mav.addObject("errorMessages", session.getAttribute("errorMessages"));
		session.removeAttribute("errorMessages");

		mav.addObject("id", id);
		// ログインユーザーオブジェクトを保管
		mav.addObject("loginUser", session.getAttribute("loginUser"));
		mav.setViewName("/NewCalculate"); // 画面遷移先を指定
		return mav;
	}

	// idがnullの場合エラーを表示
	@GetMapping("/NewCalculate/")
	public ModelAndView nullContent() {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		errorMessages.add("不正なパラメータが入力されました");
		session.setAttribute("errorMessages", errorMessages);
		mav.setViewName("redirect:/");
		return mav;
	}

	// 収支登録処理
	@PostMapping("/NewCalculate/{id}")
	public ModelAndView addContent(@PathVariable Integer id,
			@ModelAttribute("expense") Expense expense,
			@ModelAttribute("income") Income income,
			@RequestParam(name = "calculateId") int calculateId,
			@RequestParam(name = "category") int category,
			@RequestParam(name = "money") String money) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Expense newExpense = new Expense(); // form用の空のentityを準備
		Income newIncome = new Income(); // form用の空のentityを準備

		if(calculateId == 0) {
			// 日付が空白の場合
			if(expense.getDate() == null) {
				errorMessages.add("日付を入力してください");
			} else {
				String date = sdf.format(expense.getDate());
				mav.addObject("date", date);
			}
			// 備考に51文字以上入力された場合

			if (expense.getText().replaceAll("\\n", "").replaceAll("\\r", "").length() > 50) {
				errorMessages.add("備考は50文字以内で入力してください");
			}
		} else {
			// 日付が空白の場合
			if(income.getDate() == null) {
				errorMessages.add("日付を入力してください");
			} else {
				String date = sdf.format(income.getDate());
				mav.addObject("date", date);
			}
			// 備考に51文字以上入力された場合
			if (income.getText().replaceAll("\\n", "").replaceAll("\\r", "").length() > 50) {
				errorMessages.add("備考は50文字以内で入力してください");
			}
		}

		// 金額が空白の場合または半角数字でない場合
		if(!StringUtils.hasText(money)) {
			errorMessages.add("金額を入力してください");
		} else if(!(money.matches("^[0-9]+$"))) {
			errorMessages.add("金額には半角数字を入力してください");
		} else {
			Integer amount = Integer.parseInt(money);
			if(calculateId == 0) {
				newExpense.setAmount(amount);
			} else {
				newIncome.setAmount(amount);
			}
		}

		List<User> user = (List<User>) session.getAttribute("loginUser");

		if(errorMessages.size() != 0) {
			if(calculateId == 0) {
				newExpense.setExCategoryId(category);
				newExpense.setUserId(user.get(0).getId());
				newExpense.setDate(expense.getDate());
				newExpense.setText(expense.getText());
				mav.setViewName("/NewCalculate"); // 画面遷移先を指定
				List<ExpenseCategory> newCategory = expenseCategoryService.findAllExpenseCategory(); // カテゴリーを全件取得
				mav.addObject("category", newCategory);
				mav.addObject("expense", newExpense); // 値を保持
				mav.addObject("errorMessages" , errorMessages);
				mav.addObject("loginUser", session.getAttribute("loginUser"));
				mav.addObject("id", user.get(0).getId());
				mav.addObject("calculateId", calculateId);
				return mav;
			} else {
				newIncome.setInCategoryId(category);
				newIncome.setUserId(user.get(0).getId());
				newIncome.setDate(income.getDate());
				newIncome.setText(income.getText());
				mav.setViewName("/NewCalculate"); // 画面遷移先を指定
				List<IncomeCategory> newCategory = incomeCategoryService.findAllIncomeCategory(); // カテゴリーを全件取得
				mav.addObject("category", newCategory);
				mav.addObject("income", newIncome); // 値を保持
				mav.addObject("errorMessages" , errorMessages);
				mav.addObject("loginUser", session.getAttribute("loginUser"));
				mav.addObject("id", user.get(0).getId());
				mav.addObject("calculateId", calculateId);
				return mav;
			}

		}

		Integer amount = Integer.parseInt(money);

		if(calculateId == 0) {
			expense.setAmount(amount);
			expense.setUserId(user.get(0).getId());
			expense.setExCategoryId(category);
			expenseService.saveExpense(expense); // 投稿をテーブルに格納
		} else {
			income.setAmount(amount);
			income.setUserId(user.get(0).getId());
			income.setInCategoryId(category);
			incomeService.saveIncome(income); // 投稿をテーブルに格納
		}

		// ユーザーデータの取得
		User calculateUser = userService.editUser(user.get(0).getId());
		int userMoney = calculateUser.getMoney();

		// ユーザーの貯蓄額の更新
		if(calculateId == 0) {
			userMoney = userMoney - amount;
		} else {
			userMoney = userMoney + amount;
		}
		calculateUser.setMoney(userMoney);
		userService.saveUser(calculateUser);

		return new ModelAndView("redirect:/");
	}

	// 収支編集画面
	@GetMapping("/EditCalculate/{id}")
	public ModelAndView editCalculate(@PathVariable String id,
			@RequestParam(name = "calculateId") int calculateId) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		if(!id.matches("^[0-9]+$")) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			mav.setViewName("redirect:/details");
			return mav;
		}

		List<User> user = (List<User>) session.getAttribute("loginUser");
		int intId = Integer.parseInt(id);

		if(calculateId == 0) {
			Expense expense = expenseService.editExpense(intId); // 編集する支出を取得
			if(expense == null) {
				errorMessages.add("不正なパラメータが入力されました");
				session.setAttribute("errorMessages", errorMessages);
				mav.setViewName("redirect:/details");
				return mav;
			}
			if(user.get(0).getId() == expense.getUserId()) {
				mav.addObject("expense", expense); // 編集する支出をセット
				List<ExpenseCategory> category = expenseCategoryService.findAllExpenseCategory(); // カテゴリーを全件取得
				mav.addObject("category", category);
				String date = sdf.format(expense.getDate());
				mav.addObject("date", date);
			} else {
				errorMessages.add("不正なパラメータが入力されました");
				session.setAttribute("errorMessages", errorMessages);
				mav.setViewName("redirect:/details");
				return mav;
			}
		} else {
			Income income = incomeService.editIncome(intId); // 編集する支出を取得
			if(income == null) {
				errorMessages.add("不正なパラメータが入力されました");
				session.setAttribute("errorMessages", errorMessages);
				mav.setViewName("redirect:/details");
				return mav;
			}
			if(user.get(0).getId() == income.getUserId()) {
				mav.addObject("income", income); // 編集する収入をセット
				List<IncomeCategory> category = incomeCategoryService.findAllIncomeCategory(); // カテゴリーを全件取得
				mav.addObject("category", category);
				String date = sdf.format(income.getDate());
				mav.addObject("date", date);
			} else {
				errorMessages.add("不正なパラメータが入力されました");
				session.setAttribute("errorMessages", errorMessages);
				mav.setViewName("redirect:/details");
				return mav;
			}
		}

		mav.addObject("id", user.get(0).getId());
		mav.addObject("calculateId", calculateId);
		// ログインユーザーオブジェクトを保管
		mav.addObject("loginUser", session.getAttribute("loginUser"));
		mav.setViewName("/EditCalculate"); // 画面遷移先を指定
		return mav;
	}

	// idがnullの場合エラーを表示
	@GetMapping("/EditCalculate/")
	public ModelAndView nulleditContent() {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		errorMessages.add("不正なパラメータが入力されました");
		session.setAttribute("errorMessages", errorMessages);
		mav.setViewName("redirect:/details");
		return mav;
	}

	// 収支編集処理
	@PostMapping("/EditCalculate/{id}")
	public ModelAndView editContent(@PathVariable Integer id,
			@ModelAttribute("expense") Expense expense,
			@ModelAttribute("income") Income income,
			@RequestParam(name = "calculateId") int calculateId,
			@RequestParam(name = "category") int category,
			@RequestParam(name = "money") String money) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Expense newExpense = new Expense(); // form用の空のentityを準備
		Income newIncome = new Income(); // form用の空のentityを準備

		if(calculateId == 0) {
			// 日付が空白の場合
			if(expense.getDate() == null) {
				errorMessages.add("日付を入力してください");
			} else {
				String date = sdf.format(expense.getDate());
				mav.addObject("date", date);
			}
			// 備考に51文字以上入力された場合
			if (expense.getText().replaceAll("\\n", "").replaceAll("\\r", "").length() > 50) {
				errorMessages.add("備考は50文字以内で入力してください");
			}
		} else {
			// 日付が空白の場合
			if(income.getDate() == null) {
				errorMessages.add("日付を入力してください");
			} else {
				String date = sdf.format(income.getDate());
				mav.addObject("date", date);
			}
			// 備考に51文字以上入力された場合
			if (income.getText().replaceAll("\\n", "").replaceAll("\\r", "").length() > 50) {
				errorMessages.add("備考は50文字以内で入力してください");
			}
		}

		// 金額が空白の場合または半角数字でない場合
		if(!StringUtils.hasText(money)) {
			errorMessages.add("金額を入力してください");
		} else if(!(money.matches("^[0-9]+$"))) {
			errorMessages.add("金額には半角数字を入力してください");
		} else {
			Integer amount = Integer.parseInt(money);
			if(calculateId == 0) {
				newExpense.setAmount(amount);
			} else {
				newIncome.setAmount(amount);
			}
		}

		if(errorMessages.size() != 0) {
			if(calculateId == 0) {
				newExpense.setExCategoryId(category);
				newExpense.setUserId(expense.getUserId());
				newExpense.setDate(expense.getDate());
				newExpense.setText(expense.getText());
				mav.setViewName("/EditCalculate"); // 画面遷移先を指定
				List<ExpenseCategory> newCategory = expenseCategoryService.findAllExpenseCategory(); // カテゴリーを全件取得
				mav.addObject("category", newCategory);
				mav.addObject("expense", newExpense); // 値を保持
				mav.addObject("errorMessages" , errorMessages);
				mav.addObject("loginUser", session.getAttribute("loginUser"));
				mav.addObject("id", id);
				mav.addObject("calculateId", calculateId);
				return mav;
			} else {
				newIncome.setInCategoryId(category);
				newIncome.setUserId(income.getUserId());
				newIncome.setDate(income.getDate());
				newIncome.setText(income.getText());
				mav.setViewName("/EditCalculate"); // 画面遷移先を指定
				List<IncomeCategory> newCategory = incomeCategoryService.findAllIncomeCategory(); // カテゴリーを全件取得
				mav.addObject("category", newCategory);
				mav.addObject("income", newIncome); // 値を保持
				mav.addObject("errorMessages" , errorMessages);
				mav.addObject("loginUser", session.getAttribute("loginUser"));
				mav.addObject("id", id);
				mav.addObject("calculateId", calculateId);
				return mav;
			}
		}

		List<User> user = (List<User>) session.getAttribute("loginUser");

		Integer amount = Integer.parseInt(money);

		// ユーザーデータの取得
		User calculateUser = userService.editUser(user.get(0).getId());
		int userMoney = calculateUser.getMoney();

		if(calculateId == 0) {
			Expense expenseData = expenseService.editExpense(id);
			int dif = expenseData.getAmount() - amount;
			userMoney = userMoney + dif;
			expense.setAmount(amount);
			expense.setUserId(user.get(0).getId());
			expense.setExCategoryId(category);
			expenseService.saveExpense(expense); // 投稿をテーブルに格納
		} else {
			Income incomeData = incomeService.editIncome(id);
			int dif = incomeData.getAmount() - amount;
			userMoney = userMoney - dif;
			income.setAmount(amount);
			income.setUserId(user.get(0).getId());
			income.setInCategoryId(category);
			incomeService.saveIncome(income); // 投稿をテーブルに格納
		}

		calculateUser.setMoney(userMoney);
		userService.saveUser(calculateUser);

		return new ModelAndView("redirect:/details"); // rootへリダイレクト
	}

	// 収支削除処理
	@GetMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id,
			@RequestParam(name = "calculateId") int calculateId) {

		List<User> user = (List<User>) session.getAttribute("loginUser");

		// ユーザーデータの取得
		User calculateUser = userService.editUser(user.get(0).getId());
		int userMoney = calculateUser.getMoney();

		// ユーザーの貯蓄額の更新
		if(calculateId == 0) {
			Expense expenseData = expenseService.editExpense(id);
			userMoney = userMoney + expenseData.getAmount();
			expenseService.deleteExpense(id); // UrlParameterのidを基に投稿を削除
		} else {
			Income incomeData = incomeService.editIncome(id);
			userMoney = userMoney - incomeData.getAmount();
			incomeService.deleteIncome(id); // UrlParameterのidを基に投稿を削除
		}
		calculateUser.setMoney(userMoney);
		userService.saveUser(calculateUser);

		return new ModelAndView("redirect:/details"); // rootへリダイレクト
	}

}
