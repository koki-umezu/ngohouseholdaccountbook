package com.example.demo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Balance;
import com.example.demo.entity.Expense;
import com.example.demo.entity.Income;
import com.example.demo.entity.JoinExpense;
import com.example.demo.entity.JoinIncome;
import com.example.demo.entity.User;
import com.example.demo.service.ExpenseCategoryService;
import com.example.demo.service.ExpenseService;
import com.example.demo.service.IncomeService;
import com.example.demo.service.UserService;

@Controller
public class DetailsController {

	@Autowired
	ExpenseService expenseService;
	@Autowired
	IncomeService incomeService;
	@Autowired
	ExpenseCategoryService expenseCategoryService;
	@Autowired
	HttpSession session;
	@Autowired
	UserService userService;

	@GetMapping("/details")
	public ModelAndView details(String selectYear, String selectMonth) throws ParseException {
		ModelAndView mav = new ModelAndView();
		//ユーザー情報取得処理
		List<User> user = (List<User>) session.getAttribute("loginUser");
		Integer userId = user.get(0).getId();
		User userData = userService.editUser(userId);

		//エラーメッセージとパースのフォーマットの定義
		List<String> errorMessages = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");

		//-------------------------------------------------------------
		//明細一覧画面で絞込できる日付の範囲の設定処理
		//※一番古いデータがある場所まで絞り込むことが出来るようにする
		Expense ex = expenseService.findExOldest(userId);
		Income in = incomeService.findInOldest(userId);

		//ユーザー毎の収支データが存在するかチェック
		//存在する場合は日付を収支データの日付を設定
		//存在しない場合は現在の日付を設定
		Date exDate = new Date();
		Date inDate = new Date();
		if (ex != null && in != null) {
			exDate = ex.getDate();
			inDate = in.getDate();
		} else if (ex == null && in == null) {
//			errorMessages.add("収支が登録されていません");
		} else if (ex == null) {
			inDate = in.getDate();
		} else if (in == null) {
			exDate = ex.getDate();
		}

		//支出と収入の日付を比較し古いものを絞込出来る年の下限に設定
		Date serachOldestDate = new Date();
		if (exDate.after(inDate)) {
			serachOldestDate = inDate;
		} else {
			serachOldestDate = exDate;
		}
		String strOldestSearch = sdf.format(serachOldestDate);
		int searchOldestYear = Integer.parseInt(strOldestSearch.substring(0, 4));

		//現在の日付を取得し絞込出来る年の上限に設定
		Date now = new Date();
		String strNewestSearch = sdf.format(now);
		int searchNewestYear = Integer.parseInt(strNewestSearch.substring(0, 4));

		//取得した下限から上限までの範囲をリストに追加する
		List<Integer> searchYears = new ArrayList<Integer>();

		while (searchOldestYear <= searchNewestYear) {
			searchYears.add(searchNewestYear);
			searchNewestYear--;
		}
		//-------------------------------------------------------------

		//-------------------------------------------------------------
		//指定された範囲の収支データを取得する処理
		//検索する範囲の設定
		Date since = new Date();
		Date until = new Date();
		String start;

		//範囲が指定されている場合は指定された範囲をstartに設定
		if (selectYear != null) {
			start = selectYear + '-' + selectMonth + "-01 00:00:00";

			//範囲が指定されていない場合は、現在の日付の範囲をstartに設定
		} else {
			String strStart = sdf.format(since);
			selectYear = strStart.substring(0, 4);
			selectMonth = strStart.substring(5, 7);
			start = selectYear + "-" + selectMonth + "-01 00:00:00";
		}

		String yearMonth = selectYear + "年" + selectMonth + "月";
		since = sdf.parse(start);

		//設定されたstartからuntilを設定する処理
		//※カレンダーインスタンスを生成し、その月の末日を設定
		Calendar dateInstance = Calendar.getInstance();
		dateInstance.setTime(since);
		int untilDate = dateInstance.getActualMaximum(Calendar.DAY_OF_MONTH);
		String strSearchUntil = selectYear + '-' + selectMonth + '-' + String.valueOf(untilDate) + " 23:59:59";
		until = sdf.parse(strSearchUntil);

		//収支情報取得処理
		List<JoinExpense> expenses = expenseService.findBetweenDate(userId, since, until);
		List<JoinIncome> incomes = incomeService.findBetweenDate(userId, since, until);

		//収支をbalanceリストにまとめる処理とカテゴリ毎に金額をまとめる処理
		List<Balance> balances = new ArrayList<Balance>();
		Map<String, Integer> totalExpenseCategory = new LinkedHashMap<String, Integer>();
		int totalExpense = 0;
		Map<String, Integer> totalIncomeCategory = new LinkedHashMap<String, Integer>();
		int totalIncome = 0;

		//支出情報を一つずつ取り出しbalanceリストに詰め込む処理と
		//カテゴリ毎にまとめる処理
		for (JoinExpense tmpExpense : expenses) {

			Balance balance = new Balance();
			int amount = tmpExpense.getAmount();
			balance.setAmount(amount);
			balance.setBalanceId(0);
			String CategoryName = tmpExpense.getExCategory().getName();
			balance.setCategory(tmpExpense.getExCategory().getName());
			balance.setDate(tmpExpense.getDate());
			balance.setText(tmpExpense.getText());
			balance.setCategoryId(tmpExpense.getId());
			balances.add(balance);

			//金額をまとめているカテゴリがマップに存在しない場合は新規にカテゴリと金額を設定
			//存在する場合はまとめて上書きする
			if (tmpExpense.getExCategory().getId() != 99) {

				if (totalExpenseCategory.containsKey(CategoryName)) {
					totalExpenseCategory.put(CategoryName, totalExpenseCategory.get(CategoryName) + amount);
				} else {
					totalExpenseCategory.put(CategoryName, amount);
				}

				//支出の合計金額に加算する処理
				totalExpense = totalExpense + amount;
			}
		}

		//収入情報を一つずつ取り出しbalanceリストに詰め込む処理と
		//カテゴリ毎にまとめる処理
		for (JoinIncome tmpIncome : incomes) {
			Balance balance = new Balance();
			int amount = tmpIncome.getAmount();
			String CategoryName = tmpIncome.getInCategory().getName();
			balance.setAmount(amount);
			balance.setBalanceId(1);
			balance.setCategory(CategoryName);
			balance.setDate(tmpIncome.getDate());
			balance.setText(tmpIncome.getText());
			balance.setCategoryId(tmpIncome.getId());
			balances.add(balance);

			//金額をまとめているカテゴリがマップに存在しない場合は新規にカテゴリと金額を設定
			//存在する場合はまとめて上書きする
			if (tmpIncome.getInCategory().getId() != 99) {
				if (totalIncomeCategory.containsKey(CategoryName)) {
					totalIncomeCategory.put(CategoryName, totalIncomeCategory.get(CategoryName) + amount);
				} else {
					totalIncomeCategory.put(CategoryName, amount);
				}
				//収入の合計金額に加算する処理
				totalIncome = totalIncome + amount;
			}
		}

		// Map.Entryのリストを作成する
		List<Entry<String, Integer>> list_expense = new ArrayList<Entry<String, Integer>>(totalExpenseCategory.entrySet());
		List<Entry<String, Integer>> list_income = new ArrayList<Entry<String, Integer>>(totalIncomeCategory.entrySet());

		Collections.sort(list_expense, new Comparator<Entry<String, Integer>>() {
			//compareを使用して値を比較する
			public int compare(Entry<String, Integer> obj1, Entry<String, Integer> obj2)
			{
				//降順
				return obj2.getValue().compareTo(obj1.getValue());
			}
		});
		Collections.sort(list_income, new Comparator<Entry<String, Integer>>() {
			//compareを使用して値を比較する
			public int compare(Entry<String, Integer> obj1, Entry<String, Integer> obj2)
			{
				//降順
				return obj2.getValue().compareTo(obj1.getValue());
			}
		});

		//まとめた収支を日付で並べ替える処理
		balances.sort((d1, d2) -> d2.getDate().compareTo(d1.getDate()));
		//-------------------------------------------------------------

		if (balances.isEmpty()) {
			errorMessages.add("収支が登録されていません");
		}

		// セッションからエラーメッセージを取得してセッションから取り除く
		mav.addObject("sessionErrorMessages", session.getAttribute("errorMessages"));
		session.removeAttribute("errorMessages");

		//mavにオブジェクトを詰め込む処理
		mav.addObject("errorMessages", errorMessages);
		mav.addObject("loginUser", userData);
		mav.addObject("yearMonth", yearMonth);
		mav.addObject("searchYears", searchYears);
		mav.addObject("details", balances);
		mav.addObject("expense", totalExpense);
		mav.addObject("income", totalIncome);
		mav.addObject("totalExpenses", list_expense);
		mav.addObject("totalIncomes", list_income);

		mav.setViewName("/details");
		return mav;
	}

}
