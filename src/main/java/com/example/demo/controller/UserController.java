package com.example.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Logic.CipherUtil;
import com.example.demo.entity.Expense;
import com.example.demo.entity.Income;
import com.example.demo.entity.User;
import com.example.demo.service.ExpenseService;
import com.example.demo.service.IncomeService;
import com.example.demo.service.UserService;

@Controller
public class UserController {

	@Autowired
	UserService userService;
	@Autowired
	ExpenseService expenseService;
	@Autowired
	IncomeService incomeService;

	@Autowired
	HttpSession session;

	//新規登録画面表示
	@GetMapping("/SignUp")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		List<User> loginUser = (List<User>) session.getAttribute("loginUser");
		if(loginUser != null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			mav.setViewName("redirect:/");
			return mav;
		}
		// form用の空のentityを準備
		User user = new User();
		// 画面遷移先を指定
		mav.setViewName("/SignUp");
		// 準備した空のentityを保管
		mav.addObject("formModel", user);
		return mav;
	}

	// アカウント登録処理
	@PostMapping("/SignUp")
	public ModelAndView addUser(@ModelAttribute("formModel") User user,
			@RequestParam(name = "passwordCheck") String passwordCheck,
			@RequestParam(name = "name") String name,
			@RequestParam(name = "saving") String saving) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();

		// アカウントを全件取得
		List<User> confirmName = userService.selectName(name);

		// ユーザー名が空白の場合
		if (!StringUtils.hasText(user.getName())) {
			errorMessages.add("ユーザー名を入力してください");
		// ユーザー名が重複している場合
		} else if (confirmName.size() == 1) {
			errorMessages.add("ユーザー名が重複しています");
		//ユーザー名が20文字を超えているの場合
		} else if(!(user.getName().length() <= 20)) {
			errorMessages.add("ユーザー名は20文字以下で入力してください");
		}

		// パスワードがスペース、空白の場合
		if (!StringUtils.hasText(user.getPassword())) {
			errorMessages.add("パスワードを入力してください");
		// パスワードが全角の場合
		} else if (!(user.getPassword().matches("^[a-zA-Z0-9]+$"))) {
			errorMessages.add("パスワードは半角英数字6文字以上で入力してください");
			errorMessages.add("パスワードは半角英数字20文字以下で入力してください");
		// パスワードが6文字満たないの場合
		} else if (!(user.getPassword().length() >= 6)) {
			errorMessages.add("パスワードは半角英数字6文字以上で入力してください");
		//パスワードが20文字超えている場合
		} else if (!(user.getPassword().length() <= 20)) {
			errorMessages.add("パスワードは半角英数字20文字以下で入力してください");
		}
		// パスワードと確認パスワードが同一でないの場合
		if (!(user.getPassword().equals(passwordCheck))) {
			errorMessages.add("入力したパスワードと確認パスワードが一致しません");
		}

		// 貯蓄額が半角数字以外(空白の場合は除く)の場合
		if (!(saving.matches("^[0-9]+$")) && saving != "") {
			errorMessages.add("貯蓄額には半角数字を入力してください");
		}

		if (errorMessages.size() != 0) {
			User userValue = new User(); // form用の空のentityを準備
			userValue.setName(user.getName());
			userValue.setGender(user.getGender());
			userValue.setAge(user.getAge());
			mav.addObject("saving", saving);
			mav.addObject("formModel", userValue); // 値を保持
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("/SignUp");
			return mav;
		}

		// 貯蓄額が未記入の場合
		if(saving == "") {
			user.setMoney(0);
		// 貯蓄額に数字が入力された場合
		} else {
			Integer money = Integer.parseInt(saving);
			user.setMoney(money);
		}

		// 投稿をテーブルに格納
		userService.signUpUser(user);

		if(StringUtils.hasText(saving)) {
			Integer money = Integer.parseInt(saving);
			Date date = new Date();
			List<User> registerUser =userService.selectName(name);
			if(money < 0) {
				Expense expense = new Expense();
				expense.setAmount(-money);
				expense.setUserId(registerUser.get(0).getId());
				expense.setDate(date);
				expense.setExCategoryId(99);
				expense.setText("ユーザー登録");
				expenseService.saveExpense(expense); // 投稿をテーブルに格納
			} else if(money > 0){
				Income income = new Income();
				income.setAmount(money);
				income.setUserId(registerUser.get(0).getId());
				income.setDate(date);
				income.setInCategoryId(99);
				income.setText("ユーザー登録");
				incomeService.saveIncome(income); // 投稿をテーブルに格納
			}
		}

		// rootへリダイレクト
		return new ModelAndView("redirect:/Login");
	}

	// ユーザー編集画面表示
	@GetMapping("/EditUser/{id}")
	public ModelAndView editContent(@PathVariable String id) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		List<User> user_info = (List<User>) session.getAttribute("loginUser");

		// URLパターン不正の場合
		if(!id.matches("^[0-9]+$")) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			mav.setViewName("redirect:/");
			return mav;
		}

		int intId = Integer.parseInt(id);
		if(intId != user_info.get(0).getId()) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			mav.setViewName("redirect:/");
			return mav;
		}

		int userId = Integer.parseInt(id);

		// 編集するアカウントを取得
		User user = userService.editUser(userId);

		// 編集するアカウントをセット
		mav.addObject("formModel", user);
		// 編集する貯金額をセット
		mav.addObject("saving", user.getMoney());
		// 画面遷移先を指定
		mav.setViewName("/EditUser");
		return mav;
	}

	// idがnullの場合エラーを表示
	@GetMapping("/EditUser/")
	public ModelAndView nullContent() {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		errorMessages.add("不正なパラメータが入力されました");
		session.setAttribute("errorMessages", errorMessages);
		mav.setViewName("redirect:/");
		return mav;
	}

	// ユーザー編集処理
	@PostMapping("/Update/{id}")
	public ModelAndView updateContent (@PathVariable Integer id,
			@ModelAttribute("formModel") User user,
			@RequestParam(name = "password") String password,
			@RequestParam(name = "passwordCheck") String passwordCheck,
			@RequestParam(name = "name") String name,
			@RequestParam(name = "saving") String saving) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();

		// ユーザーを取得
		User userAccountData = userService.editUser(id);

		// ユーザー名の取得
		String userName = userAccountData.getName();

		// ユーザ情報を全件取得
		List<User> confirmName = userService.selectName(name);

		// UrlParameterのidを更新するentityにセット
		user.setId(id);

		// ユーザー名が空白の場合
		if (!StringUtils.hasText(user.getName())) {
			errorMessages.add("ユーザー名を入力してください");
		//ユーザー名が20文字を超えているの場合
		} else if(!(user.getName().length() <= 20)) {
			errorMessages.add("ユーザー名は20文字以下で入力してください");
		// ユーザー名が重複している場合
		} else if (confirmName.size() == 1 && !(userName.equals(name))) {
			errorMessages.add("ユーザー名が重複しています");
		}

		// パスワード、確認パスワードが空白の場合
		if (password != "" && passwordCheck != "") {
			// パスワードがスペース、空白の場合
			if (!StringUtils.hasText(password)) {
				errorMessages.add("パスワードを入力してください");
			// パスワードが全角の場合
			} else if (!(password.matches("^[a-zA-Z0-9]+$"))) {
				errorMessages.add("パスワードは半角英数字6文字以上で入力してください");
				errorMessages.add("パスワードは半角英数字20文字以下で入力してください");
			// パスワードが6文字満たないの場合
			} else if (!(password.length() >= 6)) {
				errorMessages.add("パスワードは半角英数字6文字以上で入力してください");
			//パスワードが20文字超えている場合
			} else if (!(password.length() <= 20)) {
				errorMessages.add("パスワードは半角英数字20文字以下で入力してください");
			}
		}

		// パスワードと確認パスワードが同一でないの場合
		if (!(password.equals(passwordCheck))) {
			errorMessages.add("入力したパスワードと確認パスワードが一致しません");
		}

		// パスワード、確認パスワードの入力がない場合保持
		if (password == "") {
			user.setPassword(userAccountData.getPassword());
		}else {
			String encyptPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encyptPassword);
		}

		// 貯蓄額が半角数字以外の場合
		if (!(saving.matches("^-?[0-9]+$"))) {
			errorMessages.add("貯蓄額には半角数字を入力してください");
		}

		if (errorMessages.size() != 0) {
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("loginUser", session.getAttribute("loginUser"));
			mav.addObject("saving", saving);
			mav.setViewName("/EditUser");
			return mav;
		}

		// 貯蓄額をString型からint型に変更する
		Integer money = Integer.parseInt(saving);
		// userにint型貯蓄額を入れ込む
		user.setMoney(money);

		// 変更前の貯蓄額との差額を計算する
		int dif = userAccountData.getMoney() - money;

		Date date = new Date();

		if(dif > 0) {
			Expense expense = new Expense();
			expense.setAmount(dif);
			expense.setUserId(user.getId());
			expense.setDate(date);
			expense.setExCategoryId(99);
			expense.setText("ユーザー更新");
			expenseService.saveExpense(expense); // 投稿をテーブルに格納
		} else if(dif < 0){
			Income income = new Income();
			income.setAmount(-dif);
			income.setUserId(user.getId());
			income.setDate(date);
			income.setInCategoryId(99);
			income.setText("ユーザー更新");
			incomeService.saveIncome(income); // 投稿をテーブルに格納
		}

		// 編集した情報を更新
		userService.updateUser(user);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

}
