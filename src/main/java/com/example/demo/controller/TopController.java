package com.example.demo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Logic.Search;
import com.example.demo.entity.CompareExpense;
import com.example.demo.entity.CompareIncome;
import com.example.demo.entity.Expense;
import com.example.demo.entity.Income;
import com.example.demo.entity.JoinExpense;
import com.example.demo.entity.JoinIncome;
import com.example.demo.entity.User;
import com.example.demo.service.ExpenseCategoryService;
import com.example.demo.service.ExpenseService;
import com.example.demo.service.IncomeCategoryService;
import com.example.demo.service.IncomeService;
import com.example.demo.service.UserService;

@Controller
public class TopController {

	@Autowired
	ExpenseService expenseService;

	@Autowired
	ExpenseCategoryService expenseCategoryService;

	@Autowired
	IncomeService incomeService;

	@Autowired
	IncomeCategoryService incomeCategoryService;

	@Autowired
	UserService userService;

	@Autowired
	HttpSession session;

	// 内容表示
	@GetMapping()
	public ModelAndView top(Integer selectAge, String selectYear, String selectMonth, String selectGender)
			throws ParseException {
		ModelAndView mav = new ModelAndView();

		// ログインユーザーを取得
		List<User> user = (List<User>) session.getAttribute("loginUser");
		Integer userId = user.get(0).getId();
		User userData = userService.editUser(userId);
		List<JoinExpense> expenses = new ArrayList<JoinExpense>();
		List<JoinIncome> incomes = new ArrayList<JoinIncome>();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		Date since = new Date();
		Date until = new Date();
		String start;

		String strStart = sdf.format(since);
		String year = strStart.substring(0, 4);
		String month = strStart.substring(5, 7);
		// 収支年月度表示用
		String yearMonth = year + "年" + month + "月";

		start = year + "-" + month + "-01 00:00:00";

		since = sdf.parse(start);

		Calendar dateInstance = Calendar.getInstance();
		dateInstance.setTime(since);
		int untilDate = dateInstance.getActualMaximum(Calendar.DAY_OF_MONTH);
		String strSearchUntil = year + '-' + month + '-' + String.valueOf(untilDate) + " 23:59:59";
		until = sdf.parse(strSearchUntil);

		expenses = expenseService.findBetweenDate(userId, since, until);
		incomes = incomeService.findBetweenDate(userId, since, until);

		Map<String, Integer> totalExpenseCategory = new LinkedHashMap<String, Integer>();
		int totalExpense = 0;
		Map<String, Integer> totalIncomeCategory = new LinkedHashMap<String, Integer>();
		int totalIncome = 0;
		for (JoinExpense tmpExpense : expenses) {
			String CategoryName = tmpExpense.getExCategory().getName();
			int amount = tmpExpense.getAmount();
			if (tmpExpense.getExCategory().getId() != 99) {
				if (totalExpenseCategory.containsKey(CategoryName)) {
					totalExpenseCategory.put(CategoryName, totalExpenseCategory.get(CategoryName) + amount);
				} else {
					totalExpenseCategory.put(CategoryName, amount);
				}
				totalExpense = totalExpense + amount;
			}
		}
		for (JoinIncome tmpIncome : incomes) {
			String CategoryName = tmpIncome.getInCategory().getName();
			int amount = tmpIncome.getAmount();
			if (tmpIncome.getInCategory().getId() != 99) {

				if (totalIncomeCategory.containsKey(CategoryName)) {
					totalIncomeCategory.put(CategoryName, totalIncomeCategory.get(CategoryName) + amount);
				} else {
					totalIncomeCategory.put(CategoryName, amount);
				}
				totalIncome = totalIncome + amount;
			}
		}

		// Map.Entryのリストを作成する
		List<Entry<String, Integer>> list_expense = new ArrayList<Entry<String, Integer>>(
				totalExpenseCategory.entrySet());
		List<Entry<String, Integer>> list_income = new ArrayList<Entry<String, Integer>>(
				totalIncomeCategory.entrySet());

		Collections.sort(list_expense, new Comparator<Entry<String, Integer>>() {
			//compareを使用して値を比較する
			public int compare(Entry<String, Integer> obj1, Entry<String, Integer> obj2) {
				//降順
				return obj2.getValue().compareTo(obj1.getValue());
			}
		});
		Collections.sort(list_income, new Comparator<Entry<String, Integer>>() {
			//compareを使用して値を比較する
			public int compare(Entry<String, Integer> obj1, Entry<String, Integer> obj2) {
				//降順
				return obj2.getValue().compareTo(obj1.getValue());
			}
		});

		//----------------------------------------------------------------------
		//年代別にユーザーの平均値を取得する処理
		//範囲が指定されている場合は指定された範囲をstartに設定
		if(selectYear!=null) {


		if (selectYear != null) {
			start = selectYear + '-' + selectMonth + "-01 00:00:00";

			//範囲が指定されていない場合は、現在の日付の範囲をstartに設定
		} else {
			strStart = sdf.format(since);
			selectYear = strStart.substring(0, 4);
			selectMonth = strStart.substring(5, 7);
			start = selectYear + "-" + selectMonth + "-01 00:00:00";
		}

		since = sdf.parse(start);

		//設定されたstartからuntilを設定する処理
		dateInstance.setTime(since);
		int untilDate2 = dateInstance.getActualMaximum(Calendar.DAY_OF_MONTH);
		String strSearchUntil2 = selectYear + '-' + selectMonth + '-' + String.valueOf(untilDate2) + " 23:59:59";
		until = sdf.parse(strSearchUntil2);

		//全てのユーザーの選択された月の平均の支出を取得--------

		List<CompareExpense> compareExpenses = new ArrayList<CompareExpense>();
		//選択された期間の収支情報を取得
		compareExpenses = expenseService.findAll(since, until);

		//全てのユーザーの選択された月の平均の支出を取得--------
		Map<String, Integer> searchExpenses = new LinkedHashMap<String, Integer>();
		Map<String, Integer> searchIncomes = new LinkedHashMap<String, Integer>();
		List<CompareIncome> compareIncomes = new ArrayList<CompareIncome>();
		//選択された月のデータを全件取得
		compareIncomes = incomeService.findAll(since, until);
		//選択された年代や性別に絞込
		searchExpenses = Search.exSearch(compareExpenses, selectAge, selectGender);
		searchIncomes = Search.inSearch(compareIncomes, selectAge, selectGender);

		//-------------------------------------------------------

		//ユーザーの選択された月の収支を取得---------------------
		//ユーザーの選択された月の支出を取得
		//後で並べ替えるためtmpで取得
		Map<String, Integer> tmpUserExpenseList = new LinkedHashMap<String, Integer>();
		tmpUserExpenseList = Search.exUserSearch(compareExpenses, userId);

		//カテゴリの順番が全ユーザーの平均値のリストと同じになるように調整
		Map<String, Integer> userExpenseList = new LinkedHashMap<String, Integer>();
		for (String key : searchExpenses.keySet()) {
			if (tmpUserExpenseList.containsKey(key)) {
				userExpenseList.put(key, tmpUserExpenseList.get(key));
			} else {
				userExpenseList.put(key, 0);
			}
		}

		//ユーザーのその月の収入を取得
		//後で並べ替えるためtmpで取得
		Map<String, Integer> tmpUserIncomeList = new LinkedHashMap<String, Integer>();
		tmpUserIncomeList = Search.inUserSearch(compareIncomes, userId);

		//カテゴリの順番が全ユーザーの平均値のリストと同じになるように調整
		Map<String, Integer> userIncomeList = new LinkedHashMap<String, Integer>();
		for (String key : searchIncomes.keySet()) {
			if (tmpUserIncomeList.containsKey(key)) {
				userIncomeList.put(key, tmpUserIncomeList.get(key));
			} else {
				userIncomeList.put(key, 0);
			}
		}

		mav.addObject("searchExpense", searchExpenses);
		mav.addObject("searchIncome", searchIncomes);
		mav.addObject("userExpenseList", userExpenseList);
		mav.addObject("userIncomeList", userIncomeList);

		}
		//-----------------------------------------------------------


		// セッションからエラーメッセージを取得してセッションから取り除く
		mav.addObject("errorMessages", session.getAttribute("errorMessages"));
		session.removeAttribute("errorMessages");

		//比較で使用する選択できる年の取得
		Expense ex = expenseService.findAllExOldest();
		Income in = incomeService.findAllInOldest();

		List<Integer> searchYears = new ArrayList<Integer>();
		searchYears = Search.setSearchYear(ex, in);

		// 画面遷移先を指定
		mav.setViewName("/Top");
		// ログインユーザーオブジェクトを保管

		mav.addObject("loginUser", userData);
		mav.addObject("yearMonth", yearMonth);
		mav.addObject("searchYears", searchYears);
		mav.addObject("expense", totalExpense);
		mav.addObject("income", totalIncome);
		mav.addObject("totalExpenses", list_expense);
		mav.addObject("totalIncomes", list_income);
		mav.addObject("selectYear",selectYear);
		mav.addObject("gender", selectGender);
		mav.addObject("age", selectAge);

		return mav;
	}

}
