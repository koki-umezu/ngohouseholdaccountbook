package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.CompareIncome;

@Repository
public interface CompareIncomeRepository extends JpaRepository<CompareIncome, Integer>, JpaSpecificationExecutor<CompareIncome>  {

	List<CompareIncome> findByDateBetween(Date since,Date until);
}
