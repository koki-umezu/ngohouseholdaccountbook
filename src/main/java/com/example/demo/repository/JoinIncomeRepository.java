package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.JoinIncome;

@Repository
public interface JoinIncomeRepository extends JpaRepository<JoinIncome, Integer>, JpaSpecificationExecutor<JoinIncome>  {

	List<JoinIncome> findByDateBetweenAndUserId(Date since,Date until,int id);

}