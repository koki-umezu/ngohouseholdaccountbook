package com.example.demo.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Expense;

@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Integer>, JpaSpecificationExecutor<Expense>  {

	Expense findTop1ByUserIdOrderByDate(int id);

	Expense findTopByOrderByDate();

	@Query("select t from Expense t where t.userId = :userId")
		List<Expense>findByUserId(Integer userId);




}