package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.JoinExpense;

@Repository
public interface JoinExpenseRepository extends JpaRepository<JoinExpense, Integer>, JpaSpecificationExecutor<JoinExpense>  {

	List<JoinExpense> findByDateBetweenAndUserId(Date since,Date until,int id);

//	Page<Expense> findAll(Pageable pageable);
}