package com.example.demo.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Income;

@Repository
public interface IncomeRepository extends JpaRepository<Income, Integer>, JpaSpecificationExecutor<Income>  {

	Income findTop1ByUserIdOrderByDate(int id);

	Income findTopByOrderByDate();

	@Query("select t from Income t where t.userId = :userId")
	List<Income>findByUserId(Integer userId);
}