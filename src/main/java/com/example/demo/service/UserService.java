package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Logic.CipherUtil;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	//ユーザー情報を取得
	public List<User> selectName(String name) {
		return userRepository.findByName(name);
	}

	// ユーザー登録
	public void signUpUser(User user) {
		String encyptPassword = CipherUtil.encrypt(user.getPassword());
		user.setPassword(encyptPassword);
		userRepository.save(user);
	}

	// 収支登録、削除
	public void saveUser(User user) {
		userRepository.save(user);
	}

	// ユーザー情報取得
	public User editUser(Integer id) {
		User user = (User) userRepository.findById(id).orElse(null);
		return user;
	}

	// ユーザー情報更新
	public void updateUser(User user) {
		userRepository.save(user);
	}

}
