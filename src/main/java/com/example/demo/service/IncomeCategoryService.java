package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.IncomeCategory;
import com.example.demo.repository.IncomeCategoryRepository;

@Service
public class IncomeCategoryService {

	@Autowired
	IncomeCategoryRepository incomeCategoryRepository;

	// レコード全件取得
	public List<IncomeCategory> findAllIncomeCategory() {
		return incomeCategoryRepository.findAll();
	}

}
