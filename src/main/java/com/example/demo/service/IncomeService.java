package com.example.demo.service;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.CompareIncome;
import com.example.demo.entity.Income;
import com.example.demo.entity.JoinIncome;
import com.example.demo.repository.CompareIncomeRepository;
import com.example.demo.repository.IncomeRepository;
import com.example.demo.repository.JoinIncomeRepository;


@Service
public class IncomeService {

	@Autowired
	JoinIncomeRepository joinIncomeRepository;
	@Autowired
	IncomeRepository incomeRepository;
	@Autowired
	CompareIncomeRepository compareIncomeRepository;

	public List<JoinIncome> findBetweenDate(Integer id, Date since,Date until){
		return joinIncomeRepository.findByDateBetweenAndUserId(since,until,id);
	}

	public List<CompareIncome> findAll(Date since,Date until){
		return compareIncomeRepository.findByDateBetween(since,until);
	}

	public Income findInOldest(int id) {
		return incomeRepository.findTop1ByUserIdOrderByDate(id);
	}

	public Income findAllInOldest() {
		return incomeRepository.findTopByOrderByDate();
	}

	// レコード全件取得
	public List<Income> findAllIncome() {
		return incomeRepository.findAll();
	}

	// レコード追加
	public void saveIncome(Income income) {
		incomeRepository.save(income);
	}

	// レコード一括追加
	@Transactional
	public void saveIncomeList(List<Income> incomeList) {
		for(int i = 0 ; i < incomeList.size() ; i++ ) {
			incomeRepository.save(incomeList.get(i));
		}
	}

	//レコード削除
	public void deleteIncome(Integer id) {
		incomeRepository.deleteById(id);
	}

	//レコード1件取得
	public Income editIncome(Integer id) {
		Income income = (Income) incomeRepository.findById(id).orElse(null);
		return income;
	}

	// ユーザーIDで収支を取得
	public List<Income> findByUserId(Integer userId) {
		return incomeRepository.findByUserId(userId);
	}

}
