package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.ExpenseCategory;
import com.example.demo.repository.ExpenseCategoryRepository;

@Service
public class ExpenseCategoryService {

	@Autowired
	ExpenseCategoryRepository expenseCategoryRepository;

	// レコード全件取得
	public List<ExpenseCategory> findAllExpenseCategory() {
		return expenseCategoryRepository.findAll();
	}

}
