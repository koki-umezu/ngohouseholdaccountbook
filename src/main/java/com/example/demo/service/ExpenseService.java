package com.example.demo.service;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.CompareExpense;
import com.example.demo.entity.Expense;
import com.example.demo.entity.JoinExpense;
import com.example.demo.repository.CompareExpenseRepository;
import com.example.demo.repository.ExpenseRepository;
import com.example.demo.repository.JoinExpenseRepository;


@Service
public class ExpenseService {

	@Autowired
	JoinExpenseRepository joinExpenseRepository;
	@Autowired
	ExpenseRepository expenseRepository;
	@Autowired
	CompareExpenseRepository compareExpenseRepository;

	public List<JoinExpense> findBetweenDate(Integer id, Date since,Date until){
		return joinExpenseRepository.findByDateBetweenAndUserId(since,until,id);
	}

	public List<CompareExpense> findAll(Date since,Date until){
		return compareExpenseRepository.findByDateBetween(since,until);
	}

	public Expense findExOldest(int id) {
		return expenseRepository.findTop1ByUserIdOrderByDate(id);
	}

	public Expense findAllExOldest() {
		return expenseRepository.findTopByOrderByDate();
	}

	// レコード全件取得
	public List<Expense> findAllExpense() {
		return expenseRepository.findAll();
	}

	// レコード追加
	public void saveExpense(Expense expense) {
		expenseRepository.save(expense);
	}

	// レコード一括追加
	@Transactional
	public void saveExpenseList(List<Expense> expenseList) {
		for(int i = 0 ; i < expenseList.size() ; i++ ) {
			expenseRepository.save(expenseList.get(i));
		}
	}

	//レコード削除
	public void deleteExpense(Integer id) {
		expenseRepository.deleteById(id);
	}

	//レコード1件取得
	public Expense editExpense(Integer id) {
		Expense expense = (Expense) expenseRepository.findById(id).orElse(null);
		return expense;
	}

	// ユーザーIDで支出を取得
	public List<Expense> findByUserId(Integer userId) {
		return expenseRepository.findByUserId(userId);
	}

}
