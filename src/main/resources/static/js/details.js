$(function() {

	$('.delete').on('click', function() {
		let messageLog;
		let date = $(this).parents('tr').find('.detail-date')[0].textContent;
		let amount = $(this).parents('tr').find('.detail-amount')[0].textContent;
		let category = $(this).parents('tr').find('.detail-category')[0].textContent;

		messageLog = '"'+date+"\n"+category+"："+amount+'"\nの明細を削除してよろしいでしょうか？';

		let result = confirm(messageLog);

		if (result) {
			return true;
		} else {
			return false;
		}
	});

});