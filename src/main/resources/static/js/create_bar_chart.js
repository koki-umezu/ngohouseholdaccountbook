let age = document.getElementById("age").textContent;
let gender = document.getElementById("gender").textContent;

let searchLabelLength = document.getElementsByClassName("search-expense-category").length;

let expenselabel = [];
let searchExpenseData = [];
for(i=0; i<searchLabelLength; i++){
	let category = document.getElementsByClassName('search-expense-category');
	let label = category.item(i).textContent;
	expenselabel.push(label);
	let amount = document.getElementsByClassName('search-amount-expense');
	let data = amount.item(i).textContent;
	searchExpenseData.push(data);
}

let userExpenseLabel = [];
let userExpenseData = [];
for(i=0; i<searchLabelLength; i++){
	let category = document.getElementsByClassName('user-expense-category');
	let amount = document.getElementsByClassName('user-amount-expense');
	let data = amount.item(i).textContent;
	userExpenseData.push(data);
}

var ctx = document.getElementById('ex_chart');

var data = {
    labels: expenselabel,
    datasets: [{
        label: age+gender+"のユーザー",
        data: searchExpenseData,
        backgroundColor: "#FFBEDA"
    },
    {
        label: 'ユーザー',
        data: userExpenseData,
        backgroundColor: "#BAD3FF"
    }]
};

var options = {
	    scales: {
	        xAxes: [{
	            scaleLabel: {
	                display: false,
	                labelString: 'カテゴリ'
	            }
	        }],
	        yAxes: [{
	            ticks: {
	                min: 0,
	                userCallback: function(tick) {
	                    return tick.toString() + '円';
	                }
	            },
	            scaleLabel: {
	                display: false,
	                labelString: '金額'
	            }
	        }]
	    },
	    title: {
	    	fontSize:'24',
	        fontFamily:'DotGothic16',
	        display: true,
	        text: '支出'
	    }
	};

	var ex_chart = new Chart(ctx, {
	    type: 'bar',
	    data: data,
	    options: options
	});

	let searchIncomeLabelLength = document.getElementsByClassName("search-income-category").length;

	let incomelabel = [];
	let searchIncomeData = [];
	for(i=0; i<searchIncomeLabelLength; i++){
		let category2 = document.getElementsByClassName('search-income-category');
		let label2 = category2.item(i).textContent;
		incomelabel.push(label2);
		let amount2 = document.getElementsByClassName('search-amount-income');
		let data2 = amount2.item(i).textContent;
		searchIncomeData.push(data2);
	}

	let userIncomeLabel = [];
	let userIncomeData = [];
	for(i=0; i<searchIncomeLabelLength; i++){
		let category2 = document.getElementsByClassName('user-income-category');
		let amount2 = document.getElementsByClassName('user-amount-income');
		let data2 = amount2.item(i).textContent;
		userIncomeData.push(data2);
	}

	var ctx = document.getElementById('in_chart');

	var data = {
	    labels: incomelabel,
	    datasets: [{
	        label: age+gender+"のユーザー",
	        data: searchIncomeData,
	        backgroundColor: '#FFBEDA'
	    },
	    {
	        label: 'ユーザー',
	        data: userIncomeData,
	        backgroundColor: '#BAD3FF'
	    }]
	};

	var options = {
		    scales: {
		        xAxes: [{
		            scaleLabel: {
		                display: false,
		                labelString: 'カテゴリ'
		            }
		        }],
		        yAxes: [{
		            ticks: {
		                min: 0,
		                userCallback: function(tick) {
		                    return tick.toString() + '円';
		                }
		            },
		            scaleLabel: {
		                display: false,
		                labelString: '金額'
		            }
		        }]
		    },
		    title: {
		    	fontSize:'24',
		        fontFamily:'DotGothic16',
		        display: true,
		        text: '収入'
		    }
		};

		var ex_chart = new Chart(ctx, {
		    type: 'bar',
		    data: data,
		    options: options
		});