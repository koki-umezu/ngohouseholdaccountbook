function confirmBtn(){
	if(window.confirm('ユーザー登録してよろしいでしょうか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		return false; // 送信を中止
	}
}

function confirmUpdateBtn(){
	if(window.confirm('ユーザー情報を更新してよろしいでしょうか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		return false; // 送信を中止
	}
}

function pushHideButton() {
    var txtPass = document.getElementById("password");
    var btnEye = document.getElementById("buttonEye");
    if (txtPass.type === "text") {
      txtPass.type = "password";
      btnEye.className = "fa fa-eye";
    } else {
      txtPass.type = "text";
      btnEye.className = "fa fa-eye-slash";
    }
  }

function pushHideButtonAgain() {
    var txtPass = document.getElementById("passwordCheck");
    var btnEye = document.getElementById("buttonEyeAgain");
    if (txtPass.type === "text") {
      txtPass.type = "password";
      btnEye.className = "fa fa-eye";
    } else {
      txtPass.type = "text";
      btnEye.className = "fa fa-eye-slash";
    }
  }

$('input').on('change', function () {
	var file = $(this).prop('files')[0];
	$('p').text(file.name);
});
