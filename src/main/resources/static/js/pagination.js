$(function() {
  $('.pagenation_block').paginathing({//親要素のclassを記述
   perPage: 5,//1ページあたりの表示件数
   prevText:'<i class="fas fa-angle-left"></i>',//1つ前のページへ移動するボタンのテキスト
   nextText:'<i class="fas fa-angle-right"></i>',//1つ次のページへ移動するボタンのテキスト
   activeClass: 'navi-active',//現在のページ番号に任意のclassを付与できます
   firstText: '<i class="fas fa-angle-double-left"></i>', // "最初ページ"に移動するボタンのテキスト
   lastText: '<i class="fas fa-angle-double-right"></i>', // "最後のページ"に移動するボタンのテキスト

  })
});