
function compareFunc(a, b) {
	  return b-a;
	}

let expenseLabelLength = document.getElementsByClassName("expense-category").length;

let exlabel = [];
let expenseData = [];
for(i=0; i<expenseLabelLength; i++){
	let category = document.getElementsByClassName('expense-category');
	let a = category.item(i).textContent;
	exlabel.push(a);
	let amount = document.getElementsByClassName('amount-expense');
	let b = amount.item(i).textContent;
	expenseData.push(b);
}

expenseData.sort(compareFunc);

let amount_expense = document.getElementById('amount-expense').textContent;
let amount_income = document.getElementById('amount-income').textContent;

var ctx = document.getElementById("myPieChart");
  var myPieChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: exlabel,
      datasets: [{
          backgroundColor: [
        	  "#FFBEDA",
        	  "#BAD3FF",
              "#B1F9D0",
              "#DCC2FF",
              "#FFC7AF",
              "#EDFFBE",
              "#DDDDDD",
              "#FFE4B5",
              "#EEE8AA",
              "#DDA0DD",
              "#FFFFCC",
              "#FAEBD7"
          ],
          data: expenseData
      }]
    },
    options: {
    	 maintainAspectRatio: false,
    	legend: {
            position: 'bottom',
            labels: {
            	fontFamily:'DotGothic16',
            }
        },
        tooltips:{// グラフへカーソルを合わせた際の詳細表示の設定
            callbacks:{
                label: function (tooltipItem, data) {
                	number_amount_expense=amount_expense.replaceAll(',',"");
              return data.labels[tooltipItem.index]+ ": "+data.datasets[0].data[tooltipItem.index]+"円 : "+ (data.datasets[0].data[tooltipItem.index]/number_amount_expense*100).toFixed(1) + "%";// %を最後につける
            }
            },
          },
      title: {
        display: true,
        fontSize:'24',
        fontFamily:'DotGothic16',
        text: '支出：'+amount_expense
      }
    }
  });

  let incomeLabelLength = document.getElementsByClassName("income-category").length;

  let inlabel = [];
  let incomeData = [];
  for(j=0; j<incomeLabelLength; j++){
  	let category = document.getElementsByClassName('income-category');
  	let c = category.item(j).textContent;
  	inlabel.push(c);
  	let amount = document.getElementsByClassName('amount-income');
  	let d = amount.item(j).textContent;
  	incomeData.push(d);
  }
  incomeData.sort(compareFunc);

  var ctx2 = document.getElementById("myPieChart2");
  var myPieChart = new Chart(ctx2, {
	  type: 'pie',
    data: {

      labels: inlabel,
      indexLabelPlacement: "outside",
      radius: "50",
      datasets: [{
          backgroundColor: [
        	  "#FFBEDA",
        	  "#BAD3FF",
              "#B1F9D0",
              "#DCC2FF",
              "#FFC7AF",
              "#EDFFBE",
              "#DDDDDD",
              "#FFE4B5",
              "#EEE8AA",
              "#DDA0DD",
              "#FFFFCC",
              "#FAEBD7"
          ],
          data: incomeData,
      }]
    },

    options: {
    	 maintainAspectRatio: false,
        legend: {
            position: 'bottom',
            labels: {
            	fontFamily:'DotGothic16',
            }
        },
        tooltips:{// グラフへカーソルを合わせた際の詳細表示の設定
            callbacks:{
                label: function (tooltipItem, data) {
                	number_amount_income=amount_income.replaceAll(',',"");
              return data.labels[tooltipItem.index]+ ": "+data.datasets[0].data[tooltipItem.index]+"円 : "+ (data.datasets[0].data[tooltipItem.index]/number_amount_income*100).toFixed(1) + "%";// %を最後につける
            }
            },
          },
      title: {
        display: true,
        fontSize:'24',
        fontFamily:'DotGothic16',
        text: '収入：'+amount_income
      },
    }
  });

//  let expenseLabelLength2 = document.getElementsByClassName("expense-category2").length;
//
//  let exlabel2 = [];
//  let expenseData2 = [];
//  for(l=0; l<expenseLabelLength; l++){
//  	let category = document.getElementsByClassName('expense-category2');
//  	let g = category.item(l).textContent;
//  	exlabel2.push(g);
//  	let amount = document.getElementsByClassName('amount-expense2');
//  	let h = amount.item(l).textContent;
//  	expenseData2.push(h);
//  }

//  expenseData2.sort(compareFunc);
//
//  let amount_expense = document.getElementById('amount-expense').textContent;
//  let amount_income = document.getElementById('amount-income').textContent;
//
//  var ctx = document.getElementById("myPieChart4");
//    var myPieChart = new Chart(ctx, {
//      type: 'pie',
//      data: {
//        labels: exlabel,
//        datasets: [{
//            backgroundColor: [
//          	  "#FFBEDA",
//          	  "#BAD3FF",
//                "#B1F9D0",
//                "#DCC2FF",
//                "#FFC7AF",
//                "#EDFFBE",
//                "#DDDDDD",
//                "#FFE4B5",
//                "#EEE8AA",
//                "#DDA0DD",
//                "#FFFFCC",
//                "#FAEBD7"
//            ],
//            data: expenseData
//        }]
//      },
//      options: {
//      	 maintainAspectRatio: false,
//      	legend: {
//              position: 'bottom',
//              labels: {
//              	fontFamily:'DotGothic16',
//              }
//          },
//          tooltips:{// グラフへカーソルを合わせた際の詳細表示の設定
//              callbacks:{
//                  label: function (tooltipItem, data) {
//                  	number_amount_expense=amount_expense.replaceAll(',',"");
//                return data.labels[tooltipItem.index]+ ": "+data.datasets[0].data[tooltipItem.index]+"円 : "+ (data.datasets[0].data[tooltipItem.index]/number_amount_expense*100).toFixed(1) + "%";// %を最後につける
//              }
//              },
//            },
//        title: {
//          display: true,
//          fontSize:'24',
//          fontFamily:'DotGothic16',
//          text: '支出：'+amount_expense
//        }
//      }
//    });
//
//
//  let incomeLabelLength2 = document.getElementsByClassName("income-category2").length;
//
//  let inlabel2 = [];
//  let incomeData2 = [];
//  for(k=0; k<incomeLabelLength2; k++){
//  	let category = document.getElementsByClassName('income-category2');
//  	let e = category.item(k).textContent;
//  	inlabel.push(e);
//  	let amount = document.getElementsByClassName('amount-income2');
//  	let f = amount.item(k).textContent;
//  	incomeData2.push(f);
//  }
//  incomeData2.sort(compareFunc);
//
//  var ctx2 = document.getElementById("myPieChart3");
//  var myPieChart = new Chart(ctx2, {
//	  type: 'pie',
//    data: {
//
//      labels: inlabel,
//      indexLabelPlacement: "outside",
//      radius: "50",
//      datasets: [{
//          backgroundColor: [
//        	  "#FFBEDA",
//        	  "#BAD3FF",
//              "#B1F9D0",
//              "#DCC2FF",
//              "#FFC7AF",
//              "#EDFFBE",
//              "#DDDDDD",
//              "#FFE4B5",
//              "#EEE8AA",
//              "#DDA0DD",
//              "#FFFFCC",
//              "#FAEBD7"
//          ],
//          data: incomeData,
//      }]
//    },
//
//    options: {
//    	 maintainAspectRatio: false,
//        legend: {
//            position: 'bottom',
//            labels: {
//            	fontFamily:'DotGothic16',
//            }
//        },
//        tooltips:{// グラフへカーソルを合わせた際の詳細表示の設定
//            callbacks:{
//                label: function (tooltipItem, data) {
//                	number_amount_income=amount_income.replaceAll(',',"");
//              return data.labels[tooltipItem.index]+ ": "+data.datasets[0].data[tooltipItem.index]+"円 : "+ (data.datasets[0].data[tooltipItem.index]/number_amount_income*100).toFixed(1) + "%";// %を最後につける
//            }
//            },
//          },
//      title: {
//        display: true,
//        fontSize:'24',
//        fontFamily:'DotGothic16',
//        text: '収入：'+amount_income
//      },
//    }
//  });
